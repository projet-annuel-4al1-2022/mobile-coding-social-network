import 'User.dart';

class UserLogin {
  final String access_token;
  final Map<String, dynamic> user;

  const UserLogin({required this.access_token, required this.user});

  factory UserLogin.fromJson(Map<String, dynamic> json) {
    return UserLogin(access_token: json['access_token'], user: json['user']);
  }
}

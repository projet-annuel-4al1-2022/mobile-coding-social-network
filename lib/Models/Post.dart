import 'Code.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Post {
  late String title;
  late String content;
  late String authorPseudo;

  Post(
      {required this.title, required this.content, required this.authorPseudo});

  @override
  toString() => 'Post: { $title, $content, $authorPseudo }';

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        title: json['title'],
        content: json['content'],
        authorPseudo: json['author']['pseudo']);
  }
}

import 'Post.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FullPost extends StatelessWidget {
  final Post post;
  final String avatar;
  final String username;
  final String timeAgo;
  final String comments;
  final String votesUp;
  final String votesDown;
  final String score;

  const FullPost(
      {required this.post,
      required this.avatar,
      required this.username,
      required this.timeAgo,
      required this.comments,
      required this.votesUp,
      required this.votesDown,
      required this.score});

  factory FullPost.fromJson(Map<String, dynamic> json) {
    return FullPost(
        post: json['post'],
        avatar: json['avatar'],
        username: json['username'],
        timeAgo: json['timeAgo'],
        comments: json['comments'],
        votesUp: json['votesUp'],
        votesDown: json['votesDown'],
        score: json['score']);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [Divider(), postAvatar(), postBody(), Divider()],
      ),
    );
  }

  Widget postAvatar() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: CircleAvatar(
        backgroundImage: NetworkImage(this.avatar),
      ),
    );
  }

  Widget postBody() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          postHeader(),
          postText(),
          postButtons(),
        ],
      ),
    );
  }

  Widget postHeader() {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: Text(
              this.post.title,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Text(
          '@${post.authorPseudo} · $timeAgo',
          style: const TextStyle(
            color: Colors.grey,
          ),
        ),
        Spacer(),
        IconButton(
          icon: const Icon(
            FontAwesomeIcons.angleDown,
            size: 14.0,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  Widget postText() {
    return Text(
      this.post.content,
      overflow: TextOverflow.clip,
    );
  }

  Widget postButtons() {
    return Container(
      margin: const EdgeInsets.only(top: 10.0, right: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          postIconButton(FontAwesomeIcons.comment, this.comments),
          postIconButton(FontAwesomeIcons.arrowUp, this.votesUp),
          Text(this.score),
          postIconButton(FontAwesomeIcons.arrowDown, this.votesDown),
        ],
      ),
    );
  }

  Widget postIconButton(IconData icon, String text) {
    return Row(
      children: [
        Icon(
          icon,
          size: 16.0,
          color: Colors.black45,
        ),
        Container(
          margin: const EdgeInsets.all(6.0),
          child: Text(
            text,
            style: const TextStyle(
              color: Colors.black45,
              fontSize: 14.0,
            ),
          ),
        ),
      ],
    );
  }
}

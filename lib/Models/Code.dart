import 'package:csn_mobile_app/Models/Langage.dart';

class Code {
  final String title;
  final String code;
  final Langages langage;

  const Code({required this.title, required this.code, required this.langage});

  factory Code.fromJson(Map<String, dynamic> json) {
    return Code(
        title: json['title'], code: json['code'], langage: json['langage']);
  }
}

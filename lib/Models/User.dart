import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class User extends StatelessWidget {
  late String id;
  late String firstname;
  late String lastname;
  late String pseudo;
  late String biography;
  late String email;
  late String password;

  User(
      {required this.id,
      required this.firstname,
      required this.lastname,
      required this.pseudo,
      required this.biography,
      required this.email,
      required this.password});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'].toString(),
        firstname: json['firstName'],
        lastname: json['lastName'],
        pseudo: json['pseudo'],
        biography: json['biography'],
        email: json['email'],
        password: json['password']);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          userAvatar(),
          userBody(),
        ],
      ),
    );
  }

  Widget userAvatar() {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: const CircleAvatar(
        backgroundImage: NetworkImage(
            'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png'),
      ),
    );
  }

  Widget userBody() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [Divider(), userHeader(), userText(), Divider()],
      ),
    );
  }

  Widget userHeader() {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: Text(
              '$firstname $lastname',
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Spacer(),
      ],
    );
  }

  Widget userText() {
    return Text(
      this.pseudo,
      overflow: TextOverflow.clip,
    );
  }
}

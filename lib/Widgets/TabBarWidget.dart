// ignore_for_file: unnecessary_const

import 'package:csn_mobile_app/Screens/UserFollowers.dart';
import 'package:csn_mobile_app/Screens/UserFollowing.dart';
import 'package:csn_mobile_app/Screens/UserPosts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabBarWidget extends StatefulWidget {
  const TabBarWidget({Key? key}) : super(key: key);

  @override
  _TabBarWidgetState createState() => _TabBarWidgetState();
}

class _TabBarWidgetState extends State<TabBarWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: SizedBox(
        height: (MediaQuery.of(context).size.height / 2),
        width: (MediaQuery.of(context).size.width / 2),
        child: const DecoratedBox(
          decoration: const BoxDecoration(color: Colors.red),
          child: Scaffold(
            appBar: TabBar(
              labelColor: Colors.black,
              tabs: [
                Tab(text: "Posts"),
                Tab(text: "Followings"),
                Tab(text: "Followers")
              ],
            ),
            body: TabBarView(
              children: [
                UserPostsWidget(),
                UserFollowing(),
                UserFollowers(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

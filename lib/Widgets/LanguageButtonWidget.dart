import 'package:flutter/material.dart';

class LanguageButtonWidget extends StatelessWidget {
  String? dropdownvalue = 'java';

  var items = ['c', 'javascript'];

  @override
  Widget build(BuildContext context) => DropdownButton(
        value: dropdownvalue,
        icon: Icon(Icons.keyboard_arrow_down),
        items: items.map((items) {
          return DropdownMenuItem(value: items, child: Text(items));
        }).toList(),
        onChanged: (String? newValue) {
          dropdownvalue = newValue;
        },
      );
}

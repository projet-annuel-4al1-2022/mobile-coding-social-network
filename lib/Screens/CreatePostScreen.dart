import 'package:csn_mobile_app/Models/User.dart';
import 'package:flutter/material.dart';

import '../Models/Post.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'PostsScreen.dart';

class CreatePostScreen extends StatefulWidget {
  final User user;

  const CreatePostScreen({Key? key, required this.user}) : super(key: key);

  @override
  _CreatePostScreenState createState() => _CreatePostScreenState();
}

class _CreatePostScreenState extends State<CreatePostScreen> {
  Post _post = Post(title: '', content: '', authorPseudo: '');

  Future<dynamic> createPost(Post post) async {
    final response =
        await http.post(Uri.parse('http://10.0.2.2:3000/api/posts'), body: {
      "title": post.title,
      "content": post.content,
      "codes": [
        {
          "title": "post.codes.title",
          "code": "post.codes.code",
          "langage": "java"
        }
      ],
      "authorId": widget.user.id
    });
    print(response.statusCode);
    print(response.body);

    Map<String, dynamic> post_map = json.decode(response.body);
    print(post_map);

    if (response.statusCode == 201 || response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      print(Post.fromJson(jsonDecode(response.body)));
      return jsonDecode(response.body);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to create post.');
    }
  }

  // TextEditingController'
  final TextEditingController titleController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  // _formKey
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Post"),
        actions: const [],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: _buildContent(),
      ),
    );
  }

  Widget _buildContent() {
    //
    return Form(
      key: _formKey,
      //
      child: Column(
        children: [
          //
          TextFormField(
            //    ,
            //    (hint)
            decoration: const InputDecoration(
                border: OutlineInputBorder(), hintText: "Title"),
            //    TextEditingController
            controller: titleController,
          ),
          //
          const SizedBox(height: 10),
          // Expanded ,
          //
          Expanded(
            child: TextFormField(
              // maxLines: null  expands: true
              //
              maxLines: null,
              expands: true,
              textAlignVertical: TextAlignVertical.top,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Content",
              ),
              //    TextEditingController
              controller: contentController,
            ),
          ),
          ElevatedButton(
            onPressed: () {
              setState(() {
                print(titleController.text);
                print(contentController.text);
                Post post = Post(
                    title: titleController.text,
                    content: contentController.text,
                    authorPseudo: widget.user.pseudo);
                var newPost = createPost(post);
                print(newPost.toString());
                print(post);
              });
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PostsScreen(user: widget.user)),
              );
            },
            child: const Text('Add'),
          ),
        ],
      ),
    );
  }
}

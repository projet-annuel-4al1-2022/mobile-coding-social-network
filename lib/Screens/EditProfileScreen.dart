import 'dart:convert';
import 'dart:io';

import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:csn_mobile_app/Screens/UserProfileScreen.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

import '../Models/User.dart';
import '../Widgets/ProfileWidget.dart';
import '../Widgets/TextFieldWidget.dart';

import 'package:http/http.dart' as http;

class EditProfileScreen extends StatefulWidget {
  final User user;
  const EditProfileScreen({Key? key, required this.user}) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  User _user = User(
      id: '',
      firstname: '',
      lastname: '',
      pseudo: '',
      biography: '',
      email: '',
      password: '');

  Future<User> updateUser(User newUser) async {
    var userId = newUser.id;

    final response = await http
        .patch(Uri.parse('http://10.0.2.2:3000/api/users/${userId}'), body: {
      'fistName': newUser.firstname,
      'lastName': newUser.lastname,
      'pseudo': newUser.pseudo,
      'email': newUser.email,
      'biography': newUser.biography,
      'password': newUser.password
    });

    print(response.statusCode);
    print(response.body);

    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      widget.user.firstname = map['firstName'];
      widget.user.lastname = map['lastName'];
      widget.user.pseudo = map['pseudo'];
      widget.user.biography = map['biography'];
    });

    if (response.statusCode == 201 || response.statusCode == 200) {
      return User.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update user');
    }
  }

  @override
  void initState() {
    _user.id = widget.user.id;
    _user.password = widget.user.password;
    _user.email = widget.user.email;
    print(widget.user.email);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Container(
        child: Builder(
          builder: (context) => Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            UserProfileScreen(user: widget.user)),
                  );
                },
              ),
              title: const Text('Go Back'),
              backgroundColor: Colors.blue,
            ),
            body: ListView(
              padding: EdgeInsets.symmetric(horizontal: 32),
              physics: BouncingScrollPhysics(),
              children: [
                ProfileWidget(
                  imagePath:
                      'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png',
                  isEdit: true,
                  onClicked: () async {},
                ),
                const SizedBox(height: 24),
                TextFieldWidget(
                  label: 'First Name',
                  text: widget.user.firstname,
                  onChanged: (firstname) {
                    _user.firstname = firstname;
                  },
                ),
                const SizedBox(height: 24),
                TextFieldWidget(
                  label: 'Last Name',
                  text: widget.user.lastname,
                  onChanged: (lastname) {
                    print(lastname);
                    _user.lastname = lastname;
                    print(_user.lastname);
                  },
                ),
                const SizedBox(height: 24),
                TextFieldWidget(
                  label: 'Pseudo',
                  text: widget.user.pseudo,
                  onChanged: (pseudo) {
                    _user.pseudo = pseudo;
                  },
                ),
                const SizedBox(height: 24),
                TextFieldWidget(
                  label: 'Email',
                  text: widget.user.email,
                  onChanged: (email) {},
                ),
                const SizedBox(height: 24),
                TextFieldWidget(
                  label: 'Password',
                  text: widget.user.password,
                  onChanged: (password) {},
                ),
                const SizedBox(height: 24),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Save'),
                      onPressed: () {
                        print(_user);
                        updateUser(_user);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    UserProfileScreen(user: _user)));
                      },
                    )),
              ],
            ),
          ),
        ),
      );
}

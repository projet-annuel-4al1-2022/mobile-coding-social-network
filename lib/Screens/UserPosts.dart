import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:flutter/material.dart';

import '../Models/FullPost.dart';
import '../Models/Post.dart';
import 'package:http/http.dart' as http;

class UserPostsWidget extends StatefulWidget {
  const UserPostsWidget({Key? key}) : super(key: key);

  @override
  State<UserPostsWidget> createState() => _UserPostsWidgetState();
}

class _UserPostsWidgetState extends State<UserPostsWidget> {
  List<Post> _posts = [];
  List<Post> test = [];
  List<FullPost> _fullPosts = [];
  Post _post = Post(title: '', content: '', authorPseudo: '');

  Future<dynamic> getUserPosts(int userId) async {
    try {
      List<Post> posts = [];
      List<FullPost> fullPosts = [];

      final response = await http.get(
        Uri.parse('http://10.0.2.2:3000/api/users/${userId}/posts'),
      );

      print(response.statusCode);
      print(response.body);

      List<dynamic> list = json.decode(response.body);

      for (int i = 0; i < list.length; i++) {
        _post = new Post(
            title: list[i]['title'],
            content: list[i]['content'],
            authorPseudo: list[i]['author']['pseudo']);
        posts.add(_post);
      }

      for (int i = 0; i < posts.length; i++) {
        fullPosts.add(FullPost(
            post: posts[i],
            avatar:
                'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png' /*'https://thumbs.dreamstime.com/b/ic%C3%B4ne-sociale-d-utilisateur-de-m%C3%A9dias-vecteur-image-profil-avatar-par-d%C3%A9faut-potrait-social-182347582.jpg'*/,
            username: 'FlutterDev',
            timeAgo: '5m',
            comments: '0',
            votesUp: '0',
            votesDown: '0',
            score: '0'));
      }

      setState(() {
        _posts = posts;
        for (int i = 0; i < fullPosts.length; i++) {
          _fullPosts.add(fullPosts[i]);
        }
      });

      if (response.statusCode == 200) {
        return fullPosts;
      } else {
        throw Exception('Failed to retrive users posts.');
      }
    } catch (e) {
      return null;
    }
  }

  void initState() {
    // TODO: implement initState
    super.initState();
    getUserPosts(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listOfPostsBis(),
    );
  }

  Widget listOfPostsBis() {
    print(_fullPosts);
    return Container(
        color: const Color.fromRGBO(255, 255, 255, 1),
        child: ListView.builder(
            itemCount: _fullPosts.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return _fullPosts[index];
            }));
  }
}

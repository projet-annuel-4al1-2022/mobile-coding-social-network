import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:csn_mobile_app/Models/Code.dart';
import 'package:csn_mobile_app/Models/User.dart';
import 'package:csn_mobile_app/Screens/CreatePostScreen.dart';
import 'package:csn_mobile_app/Screens/SignInScreen.dart';
import 'package:csn_mobile_app/Screens/UserProfileScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';

import '../Models/FullPost.dart';
import '../Models/Post.dart';
import 'SignUpScreen.dart';

import 'package:http/http.dart' as http;

class PostsScreen extends StatefulWidget {
  final User user;

  const PostsScreen({Key? key, required this.user}) : super(key: key);

  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {
  List<Post> _posts = [];
  List<Post> test = [];
  List<FullPost> _fullPosts = [];
  Post _post = Post(title: '', content: '', authorPseudo: '');

  Future<dynamic> getPosts() async {
    List<Post> posts = [];
    List<FullPost> fullPosts = [];

    final response = await http.get(
      Uri.parse('http://10.0.2.2:3000/api/posts'),
    );
    print(response.statusCode);
    print(response.body);

    List<dynamic> list = json.decode(response.body);
    print(list);

    for (int i = 0; i < list.length; i++) {
      _post = new Post(
          title: list[i]['title'],
          content: list[i]['content'],
          authorPseudo: list[i]['author']['pseudo']);
      posts.add(_post);
    }

    for (int i = 0; i < posts.length; i++) {
      fullPosts.add(FullPost(
          post: posts[i],
          avatar:
              'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png' /*'https://thumbs.dreamstime.com/b/ic%C3%B4ne-sociale-d-utilisateur-de-m%C3%A9dias-vecteur-image-profil-avatar-par-d%C3%A9faut-potrait-social-182347582.jpg'*/,
          username: 'FlutterDev',
          timeAgo: '5m',
          comments: '0',
          votesUp: '0',
          votesDown: '0',
          score: '0'));
    }

    setState(() {
      _posts = posts;
      for (int i = 0; i < fullPosts.length; i++) {
        _fullPosts.add(fullPosts[i]);
      }
    });

    if (response.statusCode == 200) {
      return fullPosts;
    } else {
      throw Exception('Failed to retrive all posts.');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.blue,
        leading: Container(
            margin: const EdgeInsets.all(10.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          UserProfileScreen(user: widget.user)),
                );
              },
              child: const CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png' /*'https://thumbs.dreamstime.com/b/ic%C3%B4ne-sociale-d-utilisateur-de-m%C3%A9dias-vecteur-image-profil-avatar-par-d%C3%A9faut-potrait-social-182347582.jpg'*/),
              ),
            )),
        title: const Text(
          'Home',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: listOfPostsBis(),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            heroTag: 'createPost',
            child: const Icon(FontAwesomeIcons.pen),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CreatePostScreen(
                          user: widget.user,
                        )),
              );
            },
          ),
          FloatingActionButton(
            heroTag: 'logout',
            child: const Icon(Icons.logout),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SignInScreen()),
              );
            },
          )
        ],
      ),
    );
  }

  Widget buildBottomIconButton(IconData icon, Color color) {
    return IconButton(
      icon: Icon(
        icon,
        color: color,
      ),
      onPressed: () {},
    );
  }

  Widget buildPost(Post post) {
    return FullPost(
        post: post,
        avatar:
            'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png' /*'https://thumbs.dreamstime.com/b/ic%C3%B4ne-sociale-d-utilisateur-de-m%C3%A9dias-vecteur-image-profil-avatar-par-d%C3%A9faut-potrait-social-182347582.jpg'*/,
        username: 'FlutterDev',
        timeAgo: '5m',
        comments: '0',
        votesUp: '0',
        votesDown: '0',
        score: '0');
  }

  Widget listOfPosts() {
    return Container(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: ListView.separated(
        itemBuilder: (BuildContext context, int index) {
          return _fullPosts[index];
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(
          height: 0,
        ),
        itemCount: _fullPosts.length,
      ),
    );
  }

  Widget listOfPostsBis() {
    print(_fullPosts);
    return Container(
        color: const Color.fromRGBO(255, 255, 255, 1),
        child: ListView.builder(
            itemCount: _fullPosts.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return _fullPosts[index];
            }));
  }
}

import 'package:flutter/cupertino.dart';

import '../Models/User.dart';

import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:http/http.dart';
import 'package:flutter/material.dart';

import '../Models/FullPost.dart';
import '../Models/Post.dart';
import 'package:http/http.dart' as http;

class UserFollowing extends StatefulWidget {
  const UserFollowing({Key? key}) : super(key: key);

  @override
  State<UserFollowing> createState() => _UserFollowingState();
}

class _UserFollowingState extends State<UserFollowing> {
  List<User> _users = [];
  User _user = User(
      id: '',
      firstname: '',
      lastname: '',
      pseudo: '',
      biography: '',
      email: '',
      password: '');
  int following = 0;

  Future<dynamic> getFollowing(int userId) async {
    List<User> users = [];

    final response = await http.get(
      Uri.parse('http://10.0.2.2:3000/api/users/${userId}/following'),
    );

    print(response.statusCode);
    print(response.body);

    List<dynamic> list = json.decode(response.body);
    print(list);

    for (int i = 0; i < list.length; i++) {
      String pseudo = list[i]['pseudo'];

      _user = new User(
          id: '',
          firstname: list[i]['firstName'],
          lastname: list[i]['lastName'],
          pseudo: list[i]['pseudo'],
          biography: '',
          email: '',
          password: '');

      users.add(_user);
    }

    setState(() {
      // _fullPosts = fullPosts;
      for (int i = 0; i < users.length; i++) {
        _users.add(users[i]);
      }
    });
  }

  void initState() {
    super.initState();
    getFollowing(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: listOfFollowing());
  }

  Widget listOfFollowing() {
    //print();
    return Container(
        color: const Color.fromRGBO(255, 255, 255, 1),
        child: ListView.builder(
            itemCount: _users.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return _users[index];
            }));
  }
}

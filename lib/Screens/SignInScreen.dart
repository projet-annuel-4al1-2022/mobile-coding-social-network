import 'dart:convert';

import 'package:csn_mobile_app/Models/UserLogin.dart';
import 'package:csn_mobile_app/Screens/SignUpScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Models/User.dart';
import 'PostsScreen.dart';
import 'package:http/http.dart' as http;

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  late final UserLogin userLogin;

  User _user = User(
      id: '',
      firstname: '',
      lastname: '',
      pseudo: '',
      biography: '',
      email: '',
      password: '');

  Future<UserLogin> login(String email, String password) async {
    final response = await http.post(
        Uri.parse('http://10.0.2.2:3000/api/auth/login'),
        body: {'email': email, 'password': password});
    print(response.statusCode);
    print(response.body);

    Map<String, dynamic> map = json.decode(response.body);
    Map<String, dynamic> user = map['user'];

    setState(() {
      _user.id = user['id'].toString();
      _user.firstname = user['firstName'];
      _user.lastname = user['lastName'];
      _user.pseudo = user['pseudo'];
      _user.email = user['email'];
      _user.password = user['password'];
    });

    if ((response.statusCode == 201 || response.statusCode == 200) &&
        (map['access_token'] != Null)) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => PostsScreen(user: _user)));
      return UserLogin.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to login');
    }
  }

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'CSN Mobile App',
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(10),
                    child: const Text(
                      'Sign in',
                      style: TextStyle(fontSize: 20),
                    )),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    //forgot password screen
                  },
                  child: const Text(
                    'Forgot Password',
                  ),
                ),
                Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Login'),
                      onPressed: () {
                        var logedUser = login(
                            emailController.text, passwordController.text);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PostsScreen(user: _user)));
                      },
                    )),
                Row(
                  children: <Widget>[
                    const Text('Does not have an account?'),
                    TextButton(
                      child: const Text(
                        'Sign Up',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const SignUpScreen()));
                        //signup screen
                      },
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
                Row(
                  children: <Widget>[
                    const Text('Just a Visior?'),
                    TextButton(
                      child: const Text(
                        'Click here',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PostsScreen(user: _user)));
                      },
                    )
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
              ],
            )));
  }
}

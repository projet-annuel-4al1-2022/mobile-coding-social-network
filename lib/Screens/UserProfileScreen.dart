import 'package:csn_mobile_app/Screens/PostsScreen.dart';
import 'package:csn_mobile_app/Widgets/TabBarWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:csn_mobile_app/Widgets/ButtonWidget.dart';
import 'package:csn_mobile_app/Widgets/ProfileWidget.dart';
import '../Models/User.dart';
import '../Widgets/NumbersWidget.dart';
import 'EditProfileScreen.dart';
import 'SignUpScreen.dart';

class UserProfileScreen extends StatefulWidget {
  final User user;
  const UserProfileScreen({Key? key, required this.user}) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  @override
  Widget build(BuildContext context) {
    String firstname = widget.user.firstname;
    String lastname = widget.user.lastname;

    String fullname = firstname + ' ' + lastname;

    String pseudo = widget.user.pseudo;
    String email = widget.user.email;
    String password = widget.user.password;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PostsScreen(user: widget.user)),
            );
          },
        ),
        title: const Text('Go Back'),
        backgroundColor: Colors.blue,
      ),
      body: Builder(
        builder: (context) => Scaffold(
          body: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              ProfileWidget(
                imagePath:
                    'https://intecoafrique.com/wp-content/uploads/2021/06/avatardefault_92824.png', //'https://thumbs.dreamstime.com/b/ic%C3%B4ne-sociale-d-utilisateur-de-m%C3%A9dias-vecteur-image-profil-avatar-par-d%C3%A9faut-potrait-social-182347582.jpg',
                onClicked: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) =>
                            EditProfileScreen(user: widget.user)),
                  );
                },
              ),
              const SizedBox(height: 24),
              buildName(fullname, pseudo),
              const SizedBox(height: 24),
              Center(child: buildUpgradeButton()),
              const SizedBox(height: 24),
              NumbersWidget(),
              const SizedBox(height: 48),
              const TabBarWidget(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildName(String fullname, String pseudo) => Column(
        children: [
          Text(
            fullname,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          SizedBox(height: 4),
          Text(
            pseudo,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildUpgradeButton() => ButtonWidget(
        text: 'Edit Profile',
        onClicked: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EditProfileScreen(user: widget.user)),
          );
        },
      );

  Widget buildAbout() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              'About',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 16),
            Text(
              '',
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      );
}

import 'package:csn_mobile_app/Screens/CreatePostScreen.dart';
import 'package:csn_mobile_app/Screens/SignInScreen.dart';
import 'package:csn_mobile_app/Screens/SignUpScreen.dart';
import 'package:csn_mobile_app/Screens/UserPosts.dart';
import 'package:csn_mobile_app/Screens/UserProfileScreen.dart';
import 'package:csn_mobile_app/Widgets/TabBarWidget.dart';
import 'package:flutter/material.dart';

import 'Screens/PostsScreen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: SignInScreen(),
      ),
    );
  }
}
